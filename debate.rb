require_relative 'activity'
class Debate < Activity
  def initialize(name, date, start_time, end_time, speakers, moderator)
    @name = name
    @date = date
    @start_time = start_time
    @end_time = end_time
    @speakers = speakers
    @moderator = moderator    
  end

  def description
    'Debate: ' + super + ". Hablan: #{@speakers.join(',')}. Modera: #{@moderator}."
  end

end