module Time_tools

  def calc_time(date, start_time, end_time)

    range_times = (start_time..end_time)

    activities = @activities.select do |activity| 
     activity.date == date && range_times.include?(activity.start_time) || range_times.include?(activity.end_time)
    end
    activities.empty?
  end
end