require_relative 'talk'
require_relative 'workshop'
require_relative 'debate'
require_relative 'TimeTools'
class Event
  include Time_tools
	def initialize(name, start_date, end_date)
		@name = name
		@start_date = start_date
		@end_date = end_date
		@activities = []
	end

	def self.create_event(name, start_date, end_date)
		if start_date <= end_date
			Event.new(name, start_date, end_date)
		else
			'La fecha de fin debe der mayor o igual a la fecha de inicio'
		end
	end

	def details
		"#{@name} (#{@start_date} al #{@end_date})"	
	end
	
	def dates
		array_dates = []
		range = (@start_date..@end_date) 

		range.each do |date|
			array_dates << date
		end
		array_dates
	end


	def activities_details_for_date(date)
		array_activities = []

    array_act_date = @activities.select {|activity| activity.date == date}

    if array_act_date.empty?
      array_activities
    else
      array_act_date.each do |activity|
       
        array_activities << {
          time: "#{activity.start_time} a #{activity.end_time}",
          description: "#{activity.description}"
        }

      end
      array_activities
    end

  end


	def add_talk(name, date, start_time, end_time, speaker)
		@activities << Talk.new(name, date, start_time, end_time, speaker)
	end

  def add_workshop(name, date, start_time, end_time, requirements)
    @activities << Workshop.new(name, date, start_time, end_time, requirements)
  end

  def add_debate(name, date, start_time, end_time, speakers, moderators)
    @activities << Debate.new(name, date, start_time, end_time, speakers, moderators)
  end

  def is_available_time_range?(date, start_time, end_time)
    calc_time(date, start_time, end_time)
  end
end