require_relative 'activity'
class Talk < Activity
	def initialize(name, date, start_time, end_time, speaker)
		@name = name
		@date = date
		@start_time = start_time
		@end_time = end_time
		@speaker = speaker		
	end

  def description
    'Charla: ' + super + ". Habla: #{@speaker}."
  end

end