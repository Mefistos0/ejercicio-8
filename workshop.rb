require_relative 'activity'
class Workshop < Activity
attr_accessor :name, :date, :start_time, :end_time 

def initialize(name, date, start_time, end_time, requirements)
    @name = name
    @date = date
    @start_time = start_time
    @end_time = end_time
    @requirements = requirements   
  end
  
  def description
    'Taller: ' + super + ". Requerimientos: #{@requirements}."
  end
end